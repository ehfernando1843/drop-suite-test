<?php
//get directory to scan from config.ini
$ini_array = parse_ini_file("./config/config.ini");
//store the directory name into variable
$dir = $ini_array[directory];

$iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($dir), RecursiveIteratorIterator::SELF_FIRST );
$content_count = array();
foreach ( $iterator as $path ) {
    $content_count[file_get_contents($path)]++;
}
arsort($content_count);
$key = array_keys($content_count);
$value = array_values($content_count);
echo $key[1].' '.$value[1];
?>